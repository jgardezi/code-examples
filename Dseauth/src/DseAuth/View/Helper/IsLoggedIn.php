<?php

/**
 * Description of IsLoggedIn
 * This is a View Helper to provide a function that may be called from a view.
 * This Helper returns true or false on the question of whether a user is logged.
 *
 * @author jgardezi
 */
// ./module/DseAuth/src/DseAuth/View/Helper/IsLoggedIn.php

namespace DseAuth\View\Helper;

use Zend\Http\Request;
use Zend\View\Helper\AbstractHelper;

class IsLoggedIn extends AbstractHelper {

    protected $locator;

    public function __construct($service) {
        $logname = './data/log/' . date('Y-m-d') . '.log';
        $writer = new \Zend\Log\Writer\Stream($logname);
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
//        $this->logger->debug('Logger started:' . __METHOD__);

        $this->locator = $service->get('AuthService');
    }

    public function __invoke() {
//        $this->logger->debug('Executed:' . __METHOD__);
        return $this->locator->hasIdentity();
    }

}
