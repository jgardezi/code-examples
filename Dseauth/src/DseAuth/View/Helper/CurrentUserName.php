<?php

/**
 * Description of CurrentUserName
 * This is a View Helper to return the name of the current logged in user.
 *
 * @author jgardezi
 */
// ./module/DseAuth/src/DseAuth/View/Helper/CurrentUserName.php

namespace DseAuth\View\Helper;

use Zend\Http\Request;
use Zend\View\Helper\AbstractHelper;

class CurrentUserName extends AbstractHelper {

    protected $locator;
    protected $em;
    protected $sm;
    protected $company;

    public function __construct($service) {
        $logname = './data/log/' . date('Y-m-d') . '.log';
        $writer = new \Zend\Log\Writer\Stream($logname);
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
//        $this->logger->debug('Logger started:' . __METHOD__);

        $this->sm = $service;
        $this->locator = $service->get('AuthService');
        $this->em = $service->get('Doctrine\ORM\EntityManager');
    }

    public function __invoke() {
//        $this->logger->debug('Executed:' . __METHOD__);

        $config = $this->sm->get('Config');
        $this->company = $config['Company'];

        if ($this->locator->hasIdentity()) {
            $user_id = $this->locator->getIdentity();
            $user  = $this->em->getRepository('Kpi\Entity\Employee')
                                ->findBy(array('COY' => $this->company, 'USRPRF' => strtoupper($user_id),));
            $username = $user[0]->EMPNME;

        } elseif ($this->isStore()) {
            $storeNum = $this->storeNum();
            $username = $this->storeName($storeNum);
        } else {
//            $this->logger->debug(__METHOD__ . 'User is not logged in.');
            $username = 'User not logged in';
        }
        return $username;
    }

    public function isStore() {
        $request = new \Zend\Http\PhpEnvironment\RemoteAddress;
        $ip = explode('.', $request->getIpAddress());
        //if the 1st IP segment is 172, we have a store
        if ($this->company == 1) {
            if ($ip[0] == 172) {
                $this->ip = $ip;
                return true;
            } else {
                return false;
            }
        }

        if ($this->company == 2) {
            if ($ip[0] == 10 and $ip[1] == 202) {
                $this->ip = $ip;
                return true;
            } else {
                return false;
            }
        }

    }

    public function storeNum() {
        //if the 1st IP segment is 172, we have a store
        if ($this->company == 1) {
            $siteNum = ($this->ip[1] - 21) * 250 + $this->ip[2];
        }
        if ($this->company == 2) {
            $siteNum = $this->ip[2] + 500;
        }
        return $siteNum;
    }

    public function storeName($siteNum) {
//        $this->logger->debug(__METHOD__ . ':siteNum ='.$siteNum);
        $store  = $this->em->getRepository('Kpi\Entity\Site')
                                ->findBy(array('COY' => $this->company, 'STE' => $siteNum));
//        $this->logger->debug(__METHOD__ . ':store='.var_dump($store,1));
        $storename = trim($store[0]->STEDSC);
//        $this->logger->debug(__METHOD__ . ':storename='.$storename);
        return $storename;
    }
}
