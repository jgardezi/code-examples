<?php

namespace DseAuth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use DseAuth\Model\User;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\Ldap as AuthAdapter;
use Zend\Config\Reader\Ini as ConfigReader;
use Zend\Config\Config;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream as LogWriter;
use Zend\Log\Filter\Priority as LogFilter;


class AuthController extends AbstractActionController {

    protected $form;
    protected $storage;
    protected $authservice;

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }

        return $this->authservice;
    }

    public function getSessionStorage() {
        if (!$this->storage) {
            $this->storage = $this->getServiceLocator()->get('DseAuth\Model\AuthStorage');
        }

        return $this->storage;
    }

    public function getForm() {
        if (!$this->form) {
            $user = new User();
            $builder = new AnnotationBuilder();
            $this->form = $builder->createForm($user);
        }

        return $this->form;
    }

    public function loginAction() {
        //if already login, redirect to success page
        if ($this->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute('success');
        }

        $form = $this->getForm();

        return array(
            'form' => $form,
            'messages' => $this->flashmessenger()->getMessages()
        );
    }

    public function authenticateAction() {
        $form = $this->getForm();
        $redirect = 'login';

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                //check authentication...

                $username = $this->getRequest()->getPost('username');
                $password = $this->getRequest()->getPost('password');

//                $result = $this->getAuthService()->authenticate();
                $auth = new AuthenticationService();
                $configReader = new ConfigReader();
                $configData = $configReader->fromFile('./config/ldap.ini');
                $config = new Config($configData, true);

                $log_path = $config->production->ldap->log_path;
                $options = $config->production->ldap->toArray();
                unset($options['log_path']);

                $adapter = new AuthAdapter($options, $username, $password);
                $result = $auth->authenticate($adapter);
                $messages = $result->getMessages();

                if ($log_path) {

                    $logger = new Logger;
                    $writer = new LogWriter($log_path);

                    $logger->addWriter($writer);

                    $filter = new LogFilter(Logger::DEBUG);
                    $writer->addFilter($filter);

                    foreach ($messages as $i => $message) {
                        if ($i-- > 1) { // $messages[2] and up are log messages
                            $message = str_replace("\n", "\n  ", $message);
                            $logger->debug("Ldap: $i: $message");
                        }
                    }
                }

                foreach ($messages as $message) {
                    //save message temporary into flashmessenger
                    $this->flashmessenger()->addMessage($message);
                }

                if ($result->isValid()) {
                    $redirect = 'success';
                    //check if it has rememberMe :
                    if ($request->getPost('rememberme') == 1) {
                        $this->getSessionStorage()->setRememberMe(1);
                        //set storage again
                        $this->getAuthService()->setStorage($this->getSessionStorage());
                    }
                    $this->getAuthService()->setStorage($this->getSessionStorage());
                    $this->getAuthService()->getStorage()->write($request->getPost('username'));
                }
            }
        }

        return $this->redirect()->toRoute($redirect);
    }

    public function logoutAction() {
        if ($this->getAuthService()->hasIdentity()) {
            $this->getSessionStorage()->forgetMe();
            $this->getAuthService()->clearIdentity();
            $this->flashmessenger()->addMessage("You've been logged out");
        }

        return $this->redirect()->toRoute('login');
    }

}