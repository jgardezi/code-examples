<?php

namespace DseAuth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini as ConfigReader;
use Zend\Config\Config;
//use Kpi\Entity\Employee;
//use Kpi\Entity\Currency;
use Kpi\Model\Employee;

class SuccessController extends AbstractActionController {

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct() {
        ini_set('display_errors', 0);
        $filename = './data/log/' . date('Y-m-d') . '.log';
        $writer = new \Zend\Log\Writer\Stream($filename);
//        $writer = new \Zend\Log\Writer\Null;
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
        $this->logger->debug('Logger started in '. __METHOD__);
    }

    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function indexAction() {
        if (!$this->getServiceLocator()->get('AuthService')->hasIdentity()) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel();
    }

}