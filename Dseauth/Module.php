<?php

namespace DseAuth;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;
use DseAuth\View\Helper\IsLoggedIn;
use DseAuth\View\Helper\CurrentUserName;

class Module implements AutoloaderProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'DseAuth\Model\AuthStorage' => function($sm) {
                    return new \DseAuth\Model\AuthStorage('zf2');
                },
                'AuthService' => function($sm) {
                    $authService = new AuthenticationService();
                    $authService->setStorage($sm->get('DseAuth\Model\AuthStorage'));

                    return $authService;
                },
            ),
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                // the array key here is the name you will call the view helper by in your view scripts
                'isLoggedIn' => function($sm) {
                    $locator = $sm->getServiceLocator();
                    return new IsLoggedIn($locator);
                },
                'currentUserName' => function($sm) {
                    $locator = $sm->getServiceLocator();
                    return new CurrentUserName($locator);
                },
            ),
        );
    }

}
